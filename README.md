# FFChallenge

## Initialize project & run the application on device:
Follow the tutorial here: https://facebook.github.io/react-native/docs/getting-started.html

## Integration: 

### Guide to handle rest calls from React app:
https://facebook.github.io/react-native/docs/network

### Fetch receipts:
http://94.177.216.191:7072/getRecipes

### Create new receipt:
http://94.177.216.191:7072/uploadRecipe

### Postman collection (If needed):
https://www.getpostman.com/collections/061b27445f7fe5218464

## A basic example for the upload post request: 

endpoint: http://94.177.216.191:7072/uploadRecipe
request method: POST

request body: 
{
"name": "Crock Pot Roast",
"ingredients": [{"name": "beef roast",
"quantity": "1",
"type": "Meat" },
{"name": "brown gravy mix",
"quantity": "1 package",
"type": "Baking"}],
"steps": ["Place beef roast in crock pot.",
   "Mix the dried mixes together in a bowl and sprinkle over the roast.",
   "Pour the water around the roast.",
   "Cook on low for 7-9 hours."],
"imgUrl":"asdfxzyk..."}

# Remember, this is a lightweight API ,feel free to mock some data anytime matching your functionalities! :)  